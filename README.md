PROJECT KICKOFF is the first project made by Shiven Kunal.

The goals of this project are to:
- Complete the front-end functions of a website using HTML
- Complete the back-end functionality of a website using Java and Spring boot

Requirements of the project:
1) Short introduction of yourself, with or without a photo.
2) List of hyperlinks to all your projects you've done, with a short description of what you were responsible for.
3) A footer recording the numbers of users to your website, as well as time stamp showing the time since this application is up.
4) A header allowing visitors to like the page, as well as show the number of likes on the page. 

--> Build front-page to acheive tasks 1 and 2, using HTML
--> Build footer on task 3 using HTML
--> Aspect of time since application is up on task 3 can be done using Java
--> Header can be done visually using HTML (task 4)
--> Functionality of header has to be done using Java (task 4)

Monday, June 7th 2021
Worktime: 1hr + 1hr = 2hrs

Goals:
Figure out how GitLab works
Finish tasks 1 and 2

Accomplished:
Tasks 1

Pending: 
- Task 2, on HTML and front-end
- Backend, and include in sync with frontend??
